var windowHeight = window.innerHeight;
var $body = $('body');
var $canvas = $('#shape-canvas');
$body.height(windowHeight + "px");

var canvas = $canvas[0];
if(canvas.getContext) {
  var canvasWidth = parseInt($body.width()) * 0.9;
  var canvasHeight = parseInt($body.height()) * 0.6;
  canvas.width = canvasWidth;
  canvas.height = canvasHeight;
  var ctx = canvas.getContext('2d');
  var delta = 10
  drawVerticalRules(canvasWidth, canvasHeight, 10);
  drawHorizontalRules(canvasWidth, canvasHeight, 10);
}
else {
  alert('Canvas not supported. Get a decent browser. http://browsehappy.com/')
}


function drawVerticalRules(width, height, delta){
  ctx.lineWidth = 1;
  ctx.beginPath();
  ctx.strokeStyle = '#ddd';
  for(var i = 1; i < width; i++) {
    ctx.moveTo(i*delta + 0.5, 0);
    ctx.lineTo(i*delta + 0.5, height);
    ctx.stroke();
  }
};

function drawHorizontalRules(width, height, delta){
  ctx.lineWidth = 1;
  ctx.beginPath();
  ctx.strokeStyle = '#ddd';
  for(var i = 1; i < height; i++) {
    ctx.moveTo(0, i*delta + 0.5);
    ctx.lineTo(width, i*delta + 0.5);
    ctx.stroke();
  }
};