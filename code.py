import web
import os
from os import listdir
from os.path import isfile, join

render = web.template.render('templates/')

urls = (
  '/(.*)', 'index'
)

class index:
  def GET(self, shape):
    shapes = self.get_shapes()
    return render.index(shape, shapes)

  def get_shapes(self):
    shapespath = os.getcwd() + '/static/js/shapes'
    shapes = [os.path.splitext(filename)[0] for filename in listdir(shapespath) if isfile(join(shapespath, filename))]
    return shapes

if __name__ == "__main__":
  app = web.application(urls, globals())
  app.run()
